export const AugventsPNG = require('./augvents.png')
export const ShellshotsPNG = require('./shellshots.png')
export const TwitchStitchPNG = require('./twitchstitch.png')
export const TagWithMePNG = require('./TagWithMe.png')
export const RepoPNG = require('./repo.png')

export const BackgroundSVG = require('./background.svg')

export const GithubLogo = require('./github.svg')
export const LinkedInLogo = require('./linkedin.svg')

export const AugventsGIF = require('./augvents.gif')
export const TwitchStitchGIF = require('./twitchstitch1.gif')
export const TwitchStitchGIF2 = require('./twitchstitch2.gif')
export const ShellshotsGIF = require('./shellshots.gif')
export const TagWithMeGIF = require('./tagwithme.gif')
export const RepoGIF = require('./repo.gif')

export const BackButton = require('./arrow.png')
export const MenuIcon = require('./menu.png')