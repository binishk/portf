import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'

const NotFound = () => {
    return (
        <div className="notfound">
            <p>Nothing found here. </p>
            <Link to="/">Go back</Link>
        </div>
    )
}

export default NotFound