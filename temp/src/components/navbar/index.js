import React, { useState, useEffect, useRef } from "react";
import "./styles.scss";
import { withRouter } from "react-router-dom";
import { MenuIcon } from "../../images";
import Modal from "../Modal"
import ContactForm from "../contact"

const NavBar = (props) => {
  const [isSidebarVisible, setSidebarVisible] = useState(null);
  const [isContactsVisible, setContactsVisible] = useState(false)
  const [sidebarClass, setSideBarClass] = useState("");
  const [currentDiv, setCurrentDiv] = useState("home")

  const sideRef = useRef();

  const handleSideBar = (e) => {
    e.preventDefault();
    if (e.target === sideRef.current) {
      setSidebarVisible(false);
    }
  };

  useEffect(() => {
    const updateNavHeader = () => {
      setCurrentDiv(null)
      if (!isContactsVisible){
        if (window.scrollY < 0.88*window.innerHeight){
          props.history.push("/")
        } else{
          props.history.push("/#portfolio")
        }
      }
    };

    if (!isContactsVisible){
      if (currentDiv==="portfolio") {
        try {
          window.scroll({
            top: 0.9 * window.innerHeight,
            left: 0,
            behavior: "smooth",
          });
        } catch (error) {
          window.scrollTo(0, 0);
        }
      } else if (currentDiv==="home") {
        try {
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
        } catch (error) {
          window.scrollTo(0, 0);
        }
      }
      window.addEventListener("scroll", updateNavHeader);
    }
    return () => {
      window.removeEventListener("scroll", updateNavHeader);
    };
  }, [currentDiv, isContactsVisible]);

  useEffect(() => {
    if (isSidebarVisible !== null) {
      setSideBarClass(isSidebarVisible ? "slidein" : "slideout");
      if (isSidebarVisible) {
        document.body.style.overflow = "hidden"
        sideRef.current.addEventListener("click", handleSideBar);
      } else{
        document.body.style.overflow = "visible"
      }
    }

    return () => {
      sideRef.current.removeEventListener("click", handleSideBar);
    };
  }, [isSidebarVisible]);

  return (
    <React.Fragment>
      <nav className={`navbar ${window.scrollY === 0?"at-top":""}`}>
        <div className="links">
          <img
            className="menu"
            src={MenuIcon}
            alt="menu"
            onClick={() => setSidebarVisible(!isSidebarVisible)}
          />
          <button className={`nav-link ${!window.location.hash?"link-active":""}`} onClick={() => setCurrentDiv("home")}>
            Home
          </button>
          <button className="nav-link" onClick={() => {setContactsVisible(true)}}>
            Contact
          </button>
          <button className={`nav-link ${window.location.hash==="#portfolio"?"link-active":""}`} onClick={() => setCurrentDiv("portfolio")}>
            Portfolio
          </button>
        </div>
      </nav>
      <div className="navbar-filler" />
      <div ref={sideRef} className={`sidebar  ${sidebarClass}`}>
        <nav className={`sidebar-content`}>
          <ul className="sidebar-links">
            <li>
              <button className={`side-link ${!window.location.hash?"link-active":""}`} onClick={() => {setCurrentDiv("home");setSidebarVisible(false)}}>
                Home
              </button>
            </li>
            <li>
              <button className={`side-link ${window.location.hash==="#portfolio"?"link-active":""}`} onClick={() => {setCurrentDiv("portfolio");setSidebarVisible(false)}}>
                Portfolio
              </button>
            </li>
            <li>
              <button className="side-link" onClick={() => {setContactsVisible(true)}}>
                Contact
              </button>
            </li>
          <span>koiralabinish@gmail.com</span>
          </ul>
        </nav>
      </div>
      {isContactsVisible && <Modal handleClick={() => {setContactsVisible(false)}}><ContactForm /></Modal>}
    </React.Fragment>
  );
};

export default withRouter(NavBar);
