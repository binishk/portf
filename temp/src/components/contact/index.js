import React, {useState, useEffect, useRef} from 'react';
import './styles.scss';
import axios from "axios";

const Contact = () => {
    const [btnText, setBtnText] = useState("copy")
    const email = "koiralabinish@gmail.com"
    const emailRef = useRef()
    const handleCopy = () => {
      setBtnText("copied!")
      setTimeout(() => {
        setBtnText("copy")
      }, 2500)
      emailRef.current.select()
      document.execCommand('copy')
    }
    return (
        <div className="contact">
         <p className="contact-title">find me at</p>
         <ul>
           <li><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
          </svg><b>:</b>
          <input value={email} onChange={()=>{}} ref={emailRef} readOnly/>
          <button onClick={handleCopy}>{btnText}</button>
          </li>
           <li>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
            </svg><b>:</b>
            <span >(567) 319-2308</span>
           </li>
         </ul>
        </div>
    )
  }

const ContactForm = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [formSubmitStatus, setFormSubmitStatus] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);
        setFormSubmitStatus(null)
        let formData = {
            senderName: name,
            senderEmail: email,
            message: message
        }
        try {
            let res = await axios.post('https://binishk-email-sender.herokuapp.com/', formData);
            if(res.status === 200){
                setFormSubmitStatus('success');
                setName('');
                setEmail('');
                setMessage('');
                setIsLoading(false);
            }
        } catch (error) {
            setFormSubmitStatus('error');
            setIsLoading(false);
        }
    }
    
    return ( 
        <div className="contactform">
            <Contact />
            <p className="contact-title">or send a message</p>
            <form id="form" onSubmit={handleSubmit}>
                <input  type="text" name="name" placeholder="Name" required value={name} onChange={(e) => setName(e.target.value)}/>
                <input id="email" type="email" name="email" placeholder="Email" required value={email}  onChange={(e) => setEmail(e.target.value)}/>
                <textarea id="message" type="text" name="message" placeholder="Message" rows="4" cols="50" required value={message}  onChange={(e) => setMessage(e.target.value)}></textarea>
            {
                formSubmitStatus && <div className={formSubmitStatus}>
                                        {formSubmitStatus === 'success' ? 
                                        <div><p>Thank you for reaching out! </p> <p>I will get back to you shortly</p></div>
                                        : <div><p>Something went wrong.</p><p>Sorry for the inconvinience.</p></div>}
                                        </div>
                                    
            }
                <button id="submitBtn" className={`form-btn ${formSubmitStatus?"btn--update":""}`} type="submit">{isLoading?"Submitting...":"Submit"}</button>
            </form>

        </div>
     );
}
 
export default ContactForm;