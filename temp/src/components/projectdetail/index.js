import React from "react";
import "./styles.scss";
import {
  TwitchStitchGIF,
  TwitchStitchGIF2,
  AugventsGIF,
  ShellshotsGIF,
  TagWithMeGIF,
  RepoGIF
} from "../../images";

const projectsData = {
  tagwithme: {
    name: "Tag With Me",
    screenshots: [TagWithMeGIF],
    summary:
      "An application for interacting with users interested in social events.",
    stacks: [
      "React",
      "Redux",
      "Node",
      "Express",
      "PostgreSQL",
      "Socket.io",
      "Ant Design",
      "Sass",
      "TicketMasterAPI",
      "MapBoxAPI",
    ],
    features: [
      "Designed and developed responsive front-end with reusable components using React, Redux and Ant Design",
      "Developed and maintained databases and REST API using Node.js, Express and PostgreSQL",
      "Integrated TicketMaster API for fetching social events data and MapBox API for location and map data",
      "Implemented push notifications and live chat using Socket.io",
    ],
  },
  twitchstitch: {
    name: "Twitch Stitch",
    screenshots: [TwitchStitchGIF, TwitchStitchGIF2],
    summary: "React app to watch multiple Twitch streams simultaneously.",
    stacks: ["HTML5", "CSS", "JavaScript", "React", "TwitchAPI"],
    features: [
      "Designed and developed responsive front-end using vanilla JavaScript and HTML5/CSS",
      "Implemented css grid system to effectively utilize screen space and optimize for all screen sizes",
      "Integrated Twitch API for getting live feeds and comments for channels",
      "Applied styling consistent to the original Twitch theme and added night mode",
    ],
    link: "https://twitchstitch.app",
  },
  shellshots: {
    name: "ShellShots",
    screenshots: [ShellshotsGIF],
    summary:
      "An app to aid in turtle research which helps identify and track individual turtles.",
    stacks: [
      "Django",
      "Django Rest Framework",
      "Boto3",
      "React",
      "React Native",
      "Hooks",
      "MySQL",
      "Socket.io",
      "Sass",
      "Styled-components",
      "Nginx",
    ],
    features: [
      "App was presented at the 17th Annual Symposium, Turtle Survival Alliance Conference",
      "Developed a UI containing interactive maps, image upload with editing feature and user notifications",
      "Used image processing from OpenCV library to extract keypoints and descriptors from images and perform matching",
      "Implemented Django Channels and WebSockets for live notifications",
      "Integrated Django REST Framework with AWS S3 to develop a backend deployed using Gunicorn/Nginx",
    ],
    link: "http://shellshots.live",
  },
  augvents: {
    name: "Augvents",
    screenshots: [AugventsGIF],
    summary:
      "An augmented reality application to locate nearby events in real time.",
    stacks: [
      "Node",
      "React",
      "Redux",
      "Express",
      "PostgreSQL",
      "Sequelize",
      "Passport",
      "Ant Design",
      "Sass",
      "Docker",
      "S3",
      "EC2",
      "Caddy",
    ],
    features: [
      "Developed an API with endpoints for a payment system, CRUD operations for events and authentication flow",
      "Implemented server-side caching with Redis to speed up API response for more popular areas",
      "Simplified deployment by containerization of services using Docker Compose",
      "Parsed and added events from XML files by Eventful; integrated thirds party APIs: Discovery, Twitter, Mapbox",
    ],
    link: "/augvents-react",
  },
  repo: {
    name: "Object coupling visualization",
    screenshots: [RepoGIF],
    summary:
      "Visualization of highly coupled objects from software repositories",
    stacks: [
      "D3.js",
      "HTML",
      "CSS",
      "JavaScript",
    ],
    features: [
      "Developed an interface to visualize interdependecies within files and methods on three open source projects from Github",
      "Used range input to filter objects by strength (number of dependencies) withing objects",
      "Parsed highly nested json files to create multi level tree diagrams",
      "Helps to visualize the files or methods in the projects which have high level of correlation",
      "Visualize CYCC (cyclomatic complexity) of methods using a color intensity chart",
      "Credit: Daniel Schellas for providing the data"
    ],
    link: "/softwareviz.html",
  },
};

const ProjectDetail = ({ project }) => {
  const { name, summary, stacks, features, screenshots, link } = projectsData[project];
  return (
    <div className="container">
      <p className="projectTitle">{name}</p>
      <div className="underline"></div>

      <div className="content">
        <div className="right">
          <p className="summary">{summary}</p>
          <h3>Features</h3>
          <ul>
            {features &&
              features.map((feature) => {
                return <li key={feature}>{feature}</li>;
              })}
              {link && <li><a style={{color: "dodgerblue"}} href={link} target="_blank" rel="noopener noreferrer">View app</a></li>}
          </ul>

          <h3>Technologies</h3>
          <div className="technologies">
            {stacks &&
              stacks.map((stack) => {
                return (
                  <div className="technology" key={stack}>
                    {stack}
                  </div>
                );
              })}
          </div>
        </div>
        <div className="left">
          {screenshots &&
            screenshots.map((screenshot, index) => {
              return <div key={name + index} className="image-container"> <img src={screenshot} alt="" /></div>;
            })}
        </div>
      </div>
    </div>
  );
};

export default ProjectDetail;
