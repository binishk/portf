import React, { useState, useEffect, useRef } from "react";
import { BackButton } from "../../images";
import "./styles.scss";

const Modal = ({ handleClick, children }) => {
  const [isModalOpen, setModalOpen] = useState(null);
  const modalRef = useRef();

  useEffect(() => {
    const hideModal = (e) => {
      if (isModalOpen && e.target === modalRef.current) {
        setModalOpen(false);
      }
    }

    setModalOpen(children ? true : false);
    if (isModalOpen) {
      window.addEventListener("click", hideModal);
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "visible";
    }
    return () => {
      window.removeEventListener("click", hideModal);
      if (isModalOpen) handleClick();
    };
  }, [children, isModalOpen, handleClick]);

  return isModalOpen ? (
    <div ref={modalRef} className={"modal"}>
      <div className="modal-content">
        <button className="back-btn" onClick={() => setModalOpen(false)}>
          <img src={BackButton} alt="back" />
        </button>
        {children}
      </div>
    </div>
  ) : null;
};

export default Modal;
